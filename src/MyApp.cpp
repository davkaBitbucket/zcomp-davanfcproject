// CASCADE
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>

// NFC
#include <QtNfcSubset/QNdefMessage>
#include <QtNfcSubset/QNdefRecord>

// NETWORK
#include <QtNetwork>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QtNetwork/QUdpSocket>

// FILE
#include <QFile>
#include <QIODevice>

// NOTIFICATION
#include <bb/platform/Notification>
#include <bb/platform/NotificationResult>

// HEADER
#include "MyApp.hpp"

#define XML_DATA "app/native/assets/patient.xml"
#define XML_SCHEMA "app/native/assets/patient.xsd"

using namespace bb::platform;
using namespace bb::cascades;

MyApp::MyApp(QObject *parent) :
        QObject(parent)
{
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
    // set context property so in qml we use our methods
    qml->setContextProperty("cpApp", this);
    // set sound file directory
    const QString workingDir = QDir::currentPath();
    const QString dirPaths = QString::fromLatin1("file://%1/app/public/").arg(workingDir);
    qml->documentContext()->setContextProperty("cpDir", dirPaths);
    // create root object
    prRoot = qml->createRootObject<AbstractPane>();
    // initialise invoke access manager
    prInvokeManager = new bb::system::InvokeManager();
    // initialise packedobjects
    poManager.init(XML_SCHEMA);
    // create xmlDocPtr
    prDoc = poManager.createXmlDom(XML_DATA);
    // get elements from the dom
    char *id = poManager.getElementFromXML(prDoc, "/patient/id");
    char *name = poManager.getElementFromXML(prDoc, "/patient/name");
    char *dob = poManager.getElementFromXML(prDoc, "/patient/dateOfBirth");
    char *gender = poManager.getElementFromXML(prDoc, "/patient/gender");
    char *sugar = poManager.getElementFromXML(prDoc, "/patient/sugarLevel");
    char *blood = poManager.getElementFromXML(prDoc, "/patient/bloodPressure");
    // convert chars into qstrings
    QString qstrId = QString(QLatin1String(id));
    QString qstrName = QString(QLatin1String(name));
    QString qstrDob = QString(QLatin1String(dob));
    QString qstrGender = QString(QLatin1String(gender));
    QString qstrSugar = QString(QLatin1String(sugar));
    QString qstrBlood = QString(QLatin1String(blood));
    // find network page and set it to variable
    // set properties of home page
    prHomePage = prRoot->findChild<Page*>("networkPage");
    prHomePage->setProperty("aliasId", qstrId);
    prHomePage->setProperty("aliasName", qstrName);
    prHomePage->setProperty("aliasDob", qstrDob);
    prHomePage->setProperty("aliasGender", qstrGender);
    prHomePage->setProperty("aliasSugar", qstrSugar);
    prHomePage->setProperty("aliasBlood", qstrBlood);
    // create post network access manager
    prNetPostManager = new QNetworkAccessManager(this);
    // create get network access manager
    prNetGetManager = new QNetworkAccessManager(this);
    // create new qfile with path to
    pFileObj = new QFile("app/public/patient.mp3");
    // find toast object from root
    prToast = prRoot->findChild<bb::system::SystemToast*>("toast");
    // find notification object from root
    notification = prRoot->findChild<Notification*>("notify");
    // connect it to invoke request so after receiving nfc data it will call request
    bool connectResult = prInvokeManager->connect(prInvokeManager,
            SIGNAL(invoked(const bb::system::InvokeRequest&)), this,
            SLOT(receivedInvokeTarget(const bb::system::InvokeRequest&)));
    Q_UNUSED(connectResult);
    Q_ASSERT(connectResult);
    // connect get manager to onRequestFinished slot
    bool result = connect(prNetGetManager, SIGNAL(finished(QNetworkReply*)), this,
            SLOT(onRequestFinished(QNetworkReply*)));
    Q_ASSERT(result);
    Q_UNUSED(result);
    Application::instance()->setScene(prRoot);
}

// this method download mp3 from google server
void MyApp::requestFileFromNetwork(QString anId, QString aName)
{
    QString idChars = "";
    for (int i = 0; i < anId.size(); i++) {
        idChars.append((QString) anId.data()[i] + ",");
    }
    QNetworkRequest request = QNetworkRequest();
    QString url = "http://translate.google.com/translate_tts?tl=en&q=";
    url.append("Data transmission complete. Received from," + aName + ",I,D," + idChars);
    request.setUrl(QUrl(url));
    reply = prNetGetManager->get(request);
}

// this method will notify upon receiving messages
void MyApp::notify()
{
    notification->clearEffectsForAll();
    notification->deleteFromInbox();
    notification->notify();
    prToast->show();
}

// this method will download the mp3 file and save to a location
void MyApp::onRequestFinished(QNetworkReply* reply)
{
    QNetworkReply::NetworkError netError = reply->error();
    if (netError == QNetworkReply::NoError) {
        if (pFileObj->exists()) {
            pFileObj->remove();
            delete pFileObj;
            pFileObj = new QFile("app/public/patient.mp3");
        }
        if (pFileObj->open(QIODevice::ReadWrite) == false) {
            qDebug() << "Failed to open the file";
        } else {
            pFileObj->write(reply->readAll());
            //qDebug() << reply->bytesToWrite(); this debug saved me!
            //qDebug() << "HHELLO";
            pFileObj->close();
            notify();
            reply->deleteLater();
        }
    } else {
        qDebug() << "On request error";
    }
}

// this method will post http url
void MyApp::sendDataViaHttp(QString anId, QString aName, QString aDob, QString aGender,
        QString aSugar, QString aBlood)
{

    QString id = "id=" + anId;
    QString name = "&name=" + aName;
    QString dob = "&dob=" + aDob;
    QString gender = "&gender=" + aGender;
    QString sugar = "&sugar=" + aSugar;
    QString blood = "&blood=" + aBlood;
    QString qstrUrl =
            "http://192.168.1.200/workspace/NetBeansProjects/davaHealthSystem/public_html/index.php?"
                    + id + name + dob + gender + sugar + blood;

    const QUrl url(qstrUrl);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain");
    QByteArray body("");
    prNetPostManager->post(request, body);
}

// this method will change element in the patient xml file
void MyApp::changeElementInXml(QString aPath, QString aValue)
{
    QByteArray qbaPath = aPath.toLocal8Bit();
    char* path = qbaPath.data();
    QByteArray qbaValue = aValue.toLocal8Bit();
    char* value = qbaValue.data();
    poManager.changeElementInXML(prDoc, path, value);
}

// will create NFC data available
QByteArray MyApp::createXmlNdef()
{
    const char *myPdu = poManager.encode(prDoc);
    QByteArray ba(myPdu);
    return ba;
}

// this slot will receive nfc data
void MyApp::receivedInvokeTarget(const bb::system::InvokeRequest& request)
{
    if (request.mimeType() == "application/vnd.rim.nfc.ndef") {
        QByteArray data = request.data();
        QtMobilitySubset::QNdefMessage message = QtMobilitySubset::QNdefMessage::fromByteArray(
                data);
        qDebug() << "OUTPUT: raw message data: " << message.toByteArray().toHex();
        foreach (QtMobilitySubset::QNdefRecord record, message)
        {
            qDebug() << "OUTPUT TNF: " << record.typeNameFormat();
            qDebug() << "OUTPUT Type: " << record.type();
            qDebug() << "OUTPUT Id: " << record.id();
            qDebug() << "OUTPUT Payload: " << record.payload().toHex();
            // getting the payload of the nfc record
            QByteArray byteArray = record.payload();
            char *encodedChar = byteArray.data();
            xmlDocPtr doc = poManager.decode(encodedChar);

            char *id = poManager.getElementFromXML(doc, "/patient/id");
            char *name = poManager.getElementFromXML(doc, "/patient/name");
            char *dob = poManager.getElementFromXML(doc, "/patient/dateOfBirth");
            char *gender = poManager.getElementFromXML(doc, "/patient/gender");
            char *sugar = poManager.getElementFromXML(doc, "/patient/sugarLevel");
            char *blood = poManager.getElementFromXML(doc, "/patient/bloodPressure");

            QString qstrId = QString(QLatin1String(id));
            QString qstrName = QString(QLatin1String(name));
            QString qstrDob = QString(QLatin1String(dob));
            QString qstrGender = QString(QLatin1String(gender));
            QString qstrSugar = QString(QLatin1String(sugar));
            QString qstrBlood = QString(QLatin1String(blood));

            prHomePage->setProperty("aliasId", qstrId);
            prHomePage->setProperty("aliasName", qstrName);
            prHomePage->setProperty("aliasDob", qstrDob);
            prHomePage->setProperty("aliasGender", qstrGender);
            prHomePage->setProperty("aliasSugar", qstrSugar);
            prHomePage->setProperty("aliasBlood", qstrBlood);

            // SAVE XML
            poManager.saveXMLFile("app/native/assets/patient.xml", doc);

            // PLAY PATIENT RECORD
            requestFileFromNetwork(qstrId, qstrName);

            // SEND DATA VIA HTTP
            sendDataViaHttp(qstrId, qstrName, qstrDob, qstrGender, qstrSugar, qstrBlood);

            // SET ENCODED DATA
            QString qstrEncoded = QString(QLatin1String(encodedChar));
            //qDebug() << "OUTPUT: encoded " << qstrEncoded;
            prRoot->setProperty("aliasEncodedData", qstrEncoded);

            // SET XML DATA
            int buffersize;
            xmlChar *xmlbuff;
            xmlDocDumpFormatMemory(doc, &xmlbuff, &buffersize, 1);
            char * charXmlBuff = (char*) xmlbuff;
            QString qstrXML = QString(QLatin1String(charXmlBuff));
            prRoot->setProperty("aliasXmlOutput", qstrXML);
            xmlFree(xmlbuff);
            xmlFreeDoc(doc);

        }
    }
}

void MyApp::sendDataViaUdp()
{
    QUdpSocket * m_udpSocket = new QUdpSocket(this);
    const char *myPdu = poManager.encode(prDoc);
    QByteArray encodedData(myPdu);
    m_udpSocket->writeDatagram(encodedData.data(), encodedData.size(), QHostAddress::Broadcast,
            45454);
    qDebug() << "DATA SENT";
}

void MyApp::listenViaUdp()
{
    prUdpSocket = new QUdpSocket(this);
    prUdpSocket->bind(45454, QUdpSocket::ShareAddress);
    bool result = connect(prUdpSocket, SIGNAL(readyRead()), this, SLOT(processPendingDatagrams()));
    Q_ASSERT(result);
    qDebug() << "LISTENING";
}

void MyApp::processPendingDatagrams()
{
    QByteArray datagram;
    while (prUdpSocket->hasPendingDatagrams()) {
        datagram.resize(prUdpSocket->pendingDatagramSize());
        prUdpSocket->readDatagram(datagram.data(), datagram.size());
    }
    char *encodedChar = datagram.data();
    xmlDocPtr doc = poManager.decode(encodedChar);

    char *id = poManager.getElementFromXML(doc, "/patient/id");
    char *name = poManager.getElementFromXML(doc, "/patient/name");
    char *dob = poManager.getElementFromXML(doc, "/patient/dateOfBirth");
    char *gender = poManager.getElementFromXML(doc, "/patient/gender");
    char *sugar = poManager.getElementFromXML(doc, "/patient/sugarLevel");
    char *blood = poManager.getElementFromXML(doc, "/patient/bloodPressure");

    QString qstrId = QString(QLatin1String(id));
    QString qstrName = QString(QLatin1String(name));
    QString qstrDob = QString(QLatin1String(dob));
    QString qstrGender = QString(QLatin1String(gender));
    QString qstrSugar = QString(QLatin1String(sugar));
    QString qstrBlood = QString(QLatin1String(blood));

    prHomePage->setProperty("aliasId", qstrId);
    prHomePage->setProperty("aliasName", qstrName);
    prHomePage->setProperty("aliasDob", qstrDob);
    prHomePage->setProperty("aliasGender", qstrGender);
    prHomePage->setProperty("aliasSugar", qstrSugar);
    prHomePage->setProperty("aliasBlood", qstrBlood);

    // SAVE XML
    poManager.saveXMLFile("app/native/assets/patient.xml", doc);

    // PLAY PATIENT RECORD
    requestFileFromNetwork(qstrId, qstrName);

    // SEND DATA VIA HTTP
    sendDataViaHttp(qstrId, qstrName, qstrDob, qstrGender, qstrSugar, qstrBlood);

    // SET ENCODED TEXT FIELD
    QString qstrEncoded = QString(QLatin1String(encodedChar));
    //qDebug() << "OUTPUT: encoded " << qstrEncoded;
    prRoot->setProperty("aliasEncodedData", qstrEncoded);

    // SET XML TEXT FIELD
    int buffersize;
    xmlChar *xmlbuff;
    xmlDocDumpFormatMemory(doc, &xmlbuff, &buffersize, 1);
    char * charXmlBuff = (char*) xmlbuff;
    QString qstrXML = QString(QLatin1String(charXmlBuff));
    prRoot->setProperty("aliasXmlOutput", qstrXML);
    xmlFree(xmlbuff);
    xmlFreeDoc(doc);
}

MyApp::~MyApp()
{
    xmlFreeDoc(prDoc);
    delete prNetPostManager;
    delete prNetGetManager;
    delete reply;
    qDebug() << "I have been destroyed!\n";
}

