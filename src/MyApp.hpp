
#ifndef MYAPP_HPP_
#define MYAPP_HPP_

#include <QObject>
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>
#include <bb/cascades/AbstractPane>

//TEST
#include <QtNetwork>
#include <QFile>
#include <bb/platform/NotificationDialog>
#include <bb/platform/Notification>
#include <bb/cascades/Page>
#include <bb/system/SystemToast>

// INCLUDE NETWORK MANAGER
#include <QNetworkAccessManager>

// INCLUDE PO MANAGER
#include "PoManager.hpp"

class QUdpSocket;

class MyApp : public QObject {
    Q_OBJECT

private:
    bb::system::InvokeManager *prInvokeManager;
    bb::cascades::AbstractPane *prRoot;
    PoManager poManager;
    xmlDocPtr prDoc;
    QNetworkAccessManager *prNetPostManager;
    QNetworkAccessManager *prNetGetManager;
    QNetworkReply* reply;
    QFile* pFileObj;
    bb::platform::Notification* notification;
    QUdpSocket *prUdpSocket;
    bb::cascades::Page *prHomePage;
    bb::system::SystemToast *prToast;

public:
    // Note: Accepting a parent object is good-form (optional)
    MyApp(QObject *parent = 0);
    virtual ~MyApp();
    void sendDataViaHttp(QString anId, QString aName, QString aDob, QString aGender,
            QString aSugar, QString aBlood);


public slots:
    Q_INVOKABLE QByteArray createXmlNdef();
    Q_INVOKABLE void changeElementInXml(QString aPath, QString aValue);
    void receivedInvokeTarget(const bb::system::InvokeRequest& request);
    void onRequestFinished ( QNetworkReply* reply );
    Q_INVOKABLE void requestFileFromNetwork (QString anId, QString aName);
    Q_INVOKABLE void sendDataViaUdp();
    Q_INVOKABLE void listenViaUdp();
    void notify();
    void processPendingDatagrams();

};

#endif /* MYAPP_HPP_ */
