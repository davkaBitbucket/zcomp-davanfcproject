
#ifndef POMANAGER_HPP_
#define POMANAGER_HPP_

// INCLUDE C LIBS
extern "C"
{
#include <stdio.h>
#include <packedobjects/packedobjects.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
}

class PoManager {

private:
    packedobjectsContext *pc;

public:
    PoManager();
    void init(const char *aXmlSchema);
    xmlDocPtr createXmlDom(const char *aFile);
    char* encode(xmlDocPtr aDoc);
    xmlDocPtr decode(char *pdu);
    char* getElementFromXML(xmlDocPtr aDoc, char *aPath);
    void changeElementInXML(xmlDocPtr aDoc, char *aPath, char *aValue);
    void update_xpath_nodes(xmlNodeSetPtr nodes, xmlChar* value);
    void dumpDoc(xmlDocPtr aDoc);
    void saveXMLFile(char* aPath, xmlDocPtr aDoc);
    virtual ~PoManager();
};


#endif /* POMANAGER_HPP_ */
