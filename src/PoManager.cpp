
extern "C"
{
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
}

// INCLUDE PoManager HPP
#include "PoManager.hpp"

PoManager::PoManager() {
    pc = NULL;
}

void PoManager::init(const char *aXmlSchema) {
    if ((pc = init_packedobjects(aXmlSchema, 0, NO_SCHEMA_VALIDATION | NO_DATA_VALIDATION)) == NULL) {
        fprintf(stdout, "%s\n", "ERROR: failed to initialise libpackedobjects");
        fflush(stdout);
        exit(1);
    }
    fprintf(stdout, "%s\n", "OUTPUT: poManager initialised successfully");
    fflush(stdout);
}

xmlDocPtr PoManager::createXmlDom(const char *aFile) {
    xmlDocPtr doc = NULL;
    if ((doc = packedobjects_new_doc(aFile)) == NULL) {
        fprintf(stdout, "%s\n", "ERROR: did not find .xml file");
        fflush(stdout);
        exit(1);
    }
    fprintf(stdout, "%s\n", "OUTPUT: created xml dom successfully");
    fflush(stdout);
    return doc;
}

char* PoManager::encode(xmlDocPtr aDoc) {
    char *pdu = packedobjects_encode(pc, aDoc);
    if (pc->bytes == -1) {
        fprintf(stdout, "%s\n", "ERROR: failed to encode with error");
        fflush(stdout);
        exit(1);
    }
    fprintf(stdout, "%s\n", "OUTPUT: encoded successfully");
    fflush(stdout);
    return pdu;
}

xmlDocPtr PoManager::decode(char *pdu) {
    xmlDocPtr doc = packedobjects_decode(pc, pdu);
    if (pc->decode_error) {
        fprintf(stdout, "%s\n", "ERROR: failed to decode with error");
        fflush(stdout);
        exit(1);
    }
    fprintf(stdout, "%s\n", "OUTPUT: decoded successfully");
    fflush(stdout);
    return doc;
}

void PoManager::dumpDoc(xmlDocPtr aDoc) {
    packedobjects_dump_doc(aDoc);
}

char* PoManager::getElementFromXML(xmlDocPtr aDoc, char *aPath) {
    xmlXPathContext *xpathCtx = xmlXPathNewContext(aDoc);
    xmlXPathObject *xpathObj = xmlXPathEvalExpression( (xmlChar*)aPath, xpathCtx);
    xmlNode *node = xpathObj->nodesetval->nodeTab[0];
    char *foundElement = (char *)node->children->content;
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    return foundElement;
}

/* Returns 0 on success and a negative value otherwise. */
void PoManager::changeElementInXML(xmlDocPtr aDoc, char *aPath, char *aValue) {
    xmlXPathContext *xpathCtx = xmlXPathNewContext(aDoc);
    xmlXPathObject *xpathObj = xmlXPathEvalExpression( (xmlChar*)aPath, xpathCtx);
    xmlChar *value = (xmlChar*)aValue;
    /* update selected nodes */
    update_xpath_nodes(xpathObj->nodesetval, value);
    /* Cleanup of XPath data */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    // SAVE DATA TO FILE
    saveXMLFile("app/native/assets/patient.xml", aDoc);
}

void PoManager::saveXMLFile(char* aPath, xmlDocPtr aDoc) {
    xmlSaveFileEnc(aPath, aDoc, "UTF-8");
}

void PoManager::update_xpath_nodes(xmlNodeSetPtr nodes, xmlChar* value) {
    int size;
    int i;
    //assert(value);
    size = (nodes) ? nodes->nodeNr : 0;
    for(i = size - 1; i >= 0; i--) {
        //assert(nodes->nodeTab[i]);
        xmlNodeSetContent(nodes->nodeTab[i], value);
        if (nodes->nodeTab[i]->type != XML_NAMESPACE_DECL)
            nodes->nodeTab[i] = NULL;
    }
}

PoManager::~PoManager() {
    free_packedobjects(pc);
}


