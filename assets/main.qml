
import bb.cascades 1.2
import bb.system 1.2
import bb.platform 1.2

TabbedPane {
    property alias aliasXmlOutput: txaXmlOutput.text
    property alias aliasEncodedData: txaEncodedData.text
    showTabsOnActionBar: true

    Tab {
        title: "Personal"
        description: "Patient information"
        imageSource: "images/Home.png"
        content: Page {
            objectName: "networkPage"
            property alias aliasId: txfId.text
            property alias aliasName: txfName.text
            property alias aliasDob: txfDob.text
            property alias aliasGender: txfGender.text
            property alias aliasSugar: txfSugar.text
            property alias aliasBlood: txfBlood.text
            titleBar: TitleBar {
                // to make custom title bar without cannot add container
                kind: TitleBarKind.FreeForm
                kindProperties: FreeFormTitleBarKindProperties {
                    Container {
                        background: Color.create("#16B1AF")
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Label {
                            id: titleLabel
                            text: "myHealth®"
                            verticalAlignment: VerticalAlignment.Center
                            textStyle {
                                color: Color.White
                                base: SystemDefaults.TextStyles.TitleText
                                fontSize: FontSize.PercentageValue
                                fontSizeValue: 200
                                textAlign: TextAlign.Center
                            }
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        } // END OF TITLE LABEL
                    } // END OF CONTAINER
                } // END OF KIND PROPERTIES
            } // END OF TITLE BAR

            ScrollView {
                content: Container {
                    background: Color.create("#F1EFE9")
                    // FIRST CONTAINER
                    Container {
                        //background: Color.Green
                        minHeight: 250.0
                        topPadding: 20.0
                        leftPadding: 25.0
                        rightPadding: 25.0
                        bottomPadding: 45.0
                        layout: DockLayout {
                        }
                        Label {
                            text: "Welcome"
                            textStyle {
                                color: Color.create("#16B1AF")
                                fontSize: FontSize.PercentageValue
                                fontSizeValue: 120
                            }
                        }
                        Label {
                            id: timeDate
                            text: "Thu Jan 09 2014"
                            textStyle.color: Color.create("#16B1AF")
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        TextField {
                            id: txfName
                            text: "John Doe"
                            backgroundVisible: false
                            textStyle {
                                fontSize: FontSize.PercentageValue
                                fontSizeValue: 150
                            }
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Bottom
                        }
                    }
                    // DIVIDER 1
                    Container {
                        background: Color.create("#16B1AF")
                        preferredHeight: 3.0
                        horizontalAlignment: HorizontalAlignment.Fill
                    }

                    // SECOND CONTAINER
                    Container {
                        //background: Color.Red
                        topPadding: 20.0
                        leftPadding: 25.0
                        rightPadding: 25.0
                        //preferredHeight: 1280

                        // ID
                        Label {
                            text: "ID:"
                            textStyle.color: Color.create("#16B1AF")
                            verticalAlignment: VerticalAlignment.Top
                        }
                        TextField {
                            id: txfId
                            text: "id"
                            preferredWidth: 300
                            backgroundVisible: false
                            textStyle.fontSize: FontSize.Large
                            horizontalAlignment: HorizontalAlignment.Left
                        }
                        // DOB
                        Label {
                            text: "DOB:"
                            textStyle.color: Color.create("#16B1AF")
                        }
                        TextField {
                            id: txfDob
                            text: "dob"
                            backgroundVisible: false
                            preferredWidth: 300
                            textStyle.fontSize: FontSize.Large
                            horizontalAlignment: HorizontalAlignment.Left
                        }
                        // GENDER
                        Label {
                            text: "GENDER:"
                            textStyle.color: Color.create("#16B1AF")
                        }
                        TextField {
                            id: txfGender
                            text: "gender"
                            backgroundVisible: false
                            preferredWidth: 300
                            textStyle.fontSize: FontSize.Large
                            horizontalAlignment: HorizontalAlignment.Left
                        }
                        // SUGAR
                        Label {
                            text: "SUGAR LEVEL:"
                            textStyle.color: Color.create("#16B1AF")
                        }
                        TextField {
                            id: txfSugar
                            text: "sugar"
                            backgroundVisible: false
                            preferredWidth: 300
                            textStyle.fontSize: FontSize.Large
                            horizontalAlignment: HorizontalAlignment.Left
                        }
                        // BLOOD
                        Label {
                            text: "BLOOD PRESSURE:"
                            textStyle.color: Color.create("#16B1AF")
                        }
                        TextField {
                            id: txfBlood
                            text: "blood"
                            backgroundVisible: false
                            preferredWidth: 300
                            textStyle.fontSize: FontSize.Large
                            horizontalAlignment: HorizontalAlignment.Left
                        }

                    } // END OF SECOND CONTAINER
                } // END OF SCROLL VIEW
            } // END OF MAIN CONTAINER
        } // END OF PAGE
    } // END OF TAB

    Tab {
        title: "Networking"
        description: "Send data"
        imageSource: "images/ControlPanel.png"
        content: Page {
            titleBar: TitleBar {
                // to make custom title bar without cannot add container
                kind: TitleBarKind.FreeForm
                kindProperties: FreeFormTitleBarKindProperties {
                    Container {
                        background: Color.create("#16B1AF")
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Label {
                            text: "myHealth®"
                            verticalAlignment: VerticalAlignment.Center
                            textStyle {
                                color: Color.White
                                base: SystemDefaults.TextStyles.TitleText
                                fontSize: FontSize.PercentageValue
                                fontSizeValue: 200
                                textAlign: TextAlign.Center
                            }
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        } // END OF TITLE LABEL
                    } // END OF CONTAINER
                } // END OF KIND PROPERTIES
            } // END OF TITLE BAR
            ScrollView {
                content: Container {
                    background: Color.create("#F1EFE9")
                    Label {
                        text: "Send data via UDP:"
                        textStyle.color: Color.create("#16B1AF")
                        verticalAlignment: VerticalAlignment.Top
                    }
                    Button {
                        text: "SEND"
                        onClicked: {
                            cpApp.changeElementInXml("/patient/id", txfId.text);
                            cpApp.changeElementInXml("/patient/name", txfName.text);
                            cpApp.changeElementInXml("/patient/dateOfBirth", txfDob.text);
                            cpApp.changeElementInXml("/patient/gender", txfGender.text);
                            cpApp.changeElementInXml("/patient/sugarLevel", txfSugar.text);
                            cpApp.changeElementInXml("/patient/bloodPressure", txfBlood.text);
                            cpApp.sendDataViaUdp();
                        }
                    }
                    Label {
                        text: "Listen for UDP data:"
                        textStyle.color: Color.create("#16B1AF")
                        verticalAlignment: VerticalAlignment.Top
                    }
                    Button {
                        id: btnListen
                        text: "LISTEN"
                        onClicked: {
                            btnListen.enabled = false;
                            cpApp.listenViaUdp();
                        }
                    }
                    Label {
                        text: "Send via NFC:"
                        textStyle.color: Color.create("#16B1AF")
                        verticalAlignment: VerticalAlignment.Top
                    }
                    Button {
                        id: btnNfc
                        text: "NFC"
                        contextActions: [
                            ActionSet {
                                InvokeActionItem {
                                    id: textActionItem
                                    onTriggered: {
                                        if (txfId.text == "") {
                                            toast.body = "ID Cannot Be Empty!"
                                            //toast.update();
                                            toast.show();
                                        } else if (txfName.text == "") {
                                            toast.body = "Name Cannot Be Empty!"
                                            //toast.update();
                                            toast.show();
                                        } else if (txfDob.text == "") {
                                            toast.body = "DOB Cannot Be Empty!"
                                            //toast.update();
                                            toast.show();
                                        } else {
                                            cpApp.changeElementInXml("/patient/id", txfId.text);
                                            cpApp.changeElementInXml("/patient/name", txfName.text);
                                            cpApp.changeElementInXml("/patient/dateOfBirth", txfDob.text);
                                            cpApp.changeElementInXml("/patient/gender", txfGender.text);
                                            cpApp.changeElementInXml("/patient/sugarLevel", txfSugar.text);
                                            cpApp.changeElementInXml("/patient/bloodPressure", txfBlood.text);
                                            data = cpApp.createXmlNdef();
                                        }
                                    }
                                    query {
                                        mimeType: "application/davaNfc"
                                        invokeActionId: "bb.action.SHARE"
                                    }
                                }
                            }
                        ]
                    }
                    Label {
                        text: "Repeat speaking"
                        textStyle.color: Color.create("#16B1AF")
                        verticalAlignment: VerticalAlignment.Top
                    }
                    Button {
                        text: "Say"
                        onClicked: {
                            notification.clearEffectsForAll();
                            notification.deleteFromInbox();
                            notification.notify();
                            //                        notificationDialog.show();
                        }
                    }
                    // XML OUTPUT
                    Label {
                        text: "XML:"
                        textStyle.color: Color.create("#16B1AF")
                    }
                    TextArea {
                        id: txaXmlOutput
                        preferredHeight: 300
                        text: ""
                    }
                    // ENCODED DATA
                    Label {
                        text: "ENCODED DATA:"
                        textStyle.color: Color.create("#16B1AF")
                    }
                    TextArea {
                        id: txaEncodedData
                        preferredHeight: 300
                        text: ""
                    }
                } // END OF CONTAINER
            } // END OF SCROLL VIEW
        } // END OF PAGE
    } // END TAB 2
    attachedObjects: [
        SystemToast {
            id: toast
            objectName: "toast"
            body: "DATA RECEIVED"
            //icon: "asset:///images/Battery-low.png"
            onFinished: {
                if (result == SystemUiResult.TimeOut) {
                    console.log("timeout");
                }
            }
        },
        Notification {
            id: notification
            objectName: "notify"
            title: qsTr("RECEIVED")
            body: qsTr("Data")
            soundUrl: cpDir + "patient.mp3"
        }
    ]
}
