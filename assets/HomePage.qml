import bb.cascades 1.2
import bb.system 1.2
import bb.platform 1.2

Page {
    objectName: "networkPage"
    property alias aliasId: txfId.text
    property alias aliasName: txfName.text
    property alias aliasDob: txfDob.text
    property alias aliasGender: txfGender.text
    property alias aliasSugar: txfSugar.text
    property alias aliasBlood: txfBlood.text
    titleBar: TitleBar {
        // to make custom title bar without cannot add container
        kind: TitleBarKind.FreeForm
        kindProperties: FreeFormTitleBarKindProperties {
            Container {
                background: Color.create("#16B1AF")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Label {
                    id: titleLabel
                    text: "myHealth®"
                    verticalAlignment: VerticalAlignment.Center
                    textStyle {
                        color: Color.White
                        base: SystemDefaults.TextStyles.TitleText
                        fontSize: FontSize.PercentageValue
                        fontSizeValue: 200
                        textAlign: TextAlign.Center
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                } // END OF TITLE LABEL
            } // END OF CONTAINER
        } // END OF KIND PROPERTIES
    } // END OF TITLE BAR

    content: Container {
        background: Color.create("#F1EFE9")
        // FIRST CONTAINER
        Container {
            //background: Color.Green
            minHeight: 250.0
            topPadding: 20.0
            leftPadding: 25.0
            rightPadding: 25.0
            bottomPadding: 45.0
            layout: DockLayout {
            }
            Label {
                text: "Welcome"
                textStyle {
                    color: Color.create("#16B1AF")
                    fontSize: FontSize.PercentageValue
                    fontSizeValue: 120
                }
            }
            Label {
                id: timeDate
                text: "Mon Dec 30 2013"
                textStyle.color: Color.create("#16B1AF")
                horizontalAlignment: HorizontalAlignment.Right
            }
            TextField {
                id: txfName
                text: "John Doe"
                backgroundVisible: false
                textStyle {
                    fontSize: FontSize.PercentageValue
                    fontSizeValue: 150
                }
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Bottom
            }
        }
        // DIVIDER 1
        Container {
            background: Color.create("#16B1AF")
            preferredHeight: 3.0
            horizontalAlignment: HorizontalAlignment.Fill
        }
        // SECOND CONTAINER
        ScrollView {
            
            Container {
                //background: Color.Red
                topPadding: 20.0
                leftPadding: 25.0
                rightPadding: 25.0
                preferredHeight: 1280
                preferredWidth: 768

                // ID
                Label {
                    text: "ID:"
                    textStyle.color: Color.create("#16B1AF")
                    verticalAlignment: VerticalAlignment.Top
                }
                TextField {
                    id: txfId
                    text: "id"
                    preferredWidth: 300
                    //backgroundVisible: false
                    textStyle.fontSize: FontSize.Large
                    horizontalAlignment: HorizontalAlignment.Left
                }
                // DOB
                Label {
                    text: "DOB:"
                    textStyle.color: Color.create("#16B1AF")
                }
                TextField {
                    id: txfDob
                    text: "dob"
                    //backgroundVisible: false
                    preferredWidth: 300
                    textStyle.fontSize: FontSize.Large
                    horizontalAlignment: HorizontalAlignment.Left
                }
                // GENDER
                Label {
                    text: "GENDER:"
                    textStyle.color: Color.create("#16B1AF")
                }
                TextField {
                    id: txfGender
                    text: "gender"
                    //backgroundVisible: false
                    preferredWidth: 300
                    textStyle.fontSize: FontSize.Large
                    horizontalAlignment: HorizontalAlignment.Left
                }
                // SUGAR
                Label {
                    text: "SUGAR LEVEL:"
                    textStyle.color: Color.create("#16B1AF")
                }
                TextField {
                    id: txfSugar
                    text: "sugar"
                    //backgroundVisible: false
                    preferredWidth: 300
                    textStyle.fontSize: FontSize.Large
                    horizontalAlignment: HorizontalAlignment.Left
                }
                // BLOOD
                Label {
                    text: "BLOOD PRESSURE:"
                    textStyle.color: Color.create("#16B1AF")
                }
                TextField {
                    id: txfBlood
                    text: "blood"
                    //backgroundVisible: false
                    preferredWidth: 300
                    textStyle.fontSize: FontSize.Large
                    horizontalAlignment: HorizontalAlignment.Left
                }

            } // END OF SECOND CONTAINER
        } // END OF SCROLL VIEW

    } // END OF MAIN CONTAINER
    //        actions: [
    //            InvokeActionItem {
    //                id: nfcInvoker
    ////                ActionBar.placement: ActionBarPlacement.InOverflow
    //                query {
    //                    //mimeType: "application/vnd.rim.nfc.ndef"
    //                    mimeType: "application/myapp1"
    //                    invokeActionId: "bb.action.SHARE"
    //                }
    //                handler: InvokeHandler {
    //                    id: shareHandler
    //
    //                    onInvoking: {
    //                        if (txfId.text == "") {
    //                            toast.body = "ID Cannot Be Empty!"
    //                            //toast.update();
    //                            toast.show();
    //                        } else if (txfName.text == "") {
    //                            toast.body = "Name Cannot Be Empty!"
    //                            //toast.update();
    //                            toast.show();
    //                        } else if (txfDob.text == "") {
    //                            toast.body = "DOB Cannot Be Empty!"
    //                            //toast.update();
    //                            toast.show();
    //                        } else {
    //                            cpApp.changeElementInXml("/member/id", txfId.text);
    //                            cpApp.changeElementInXml("/member/name", txfName.text);
    //                            cpApp.changeElementInXml("/member/dateOfBirth", txfDob.text);
    //                            nfcInvoker.data = cpApp.createXmlNdef();
    //                            shareHandler.confirm();
    //                        }
    //                    }
    //                }
    //            }
    //        ]

    // DIALOG BOX
//    attachedObjects: [
//        SystemToast {
//            id: toast
//            body: ""
//            //icon: "asset:///images/Battery-low.png"
//            onFinished: {
//                if (result == SystemUiResult.TimeOut) {
//                    console.log("timeout");
//                }
//            }
//        },
//        Notification {
//            id: notification
//            objectName: "notify"
//            title: qsTr("TITLE")
//            body: qsTr("BODY")
//            soundUrl: cpDir + "member.mp3"
//        },
//        NotificationDialog {
//            id: notificationDialog
//            objectName: "notifyDialog"
//            title: qsTr("ALERT")
//            body: qsTr("RECEIVED NFC DATA")
//            soundUrl: cpDir + "member.mp3"
//            buttons: [
//                SystemUiButton {
//                    label: qsTr("Okay")
//                }
//            ]
//        }
//    ]
} // END OF PAGE
