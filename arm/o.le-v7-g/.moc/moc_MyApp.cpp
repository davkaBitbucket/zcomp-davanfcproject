/****************************************************************************
** Meta object code from reading C++ file 'MyApp.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/MyApp.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MyApp.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MyApp[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,    6,    7,    6, 0x0a,
      47,   34,    6,    6, 0x0a,
      91,   83,    6,    6, 0x0a,
     145,  139,    6,    6, 0x0a,
     190,  179,    6,    6, 0x0a,
     230,    6,    6,    6, 0x0a,
     247,    6,    6,    6, 0x0a,
     262,    6,    6,    6, 0x0a,
     271,    6,    6,    6, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MyApp[] = {
    "MyApp\0\0QByteArray\0createXmlNdef()\0"
    "aPath,aValue\0changeElementInXml(QString,QString)\0"
    "request\0receivedInvokeTarget(bb::system::InvokeRequest)\0"
    "reply\0onRequestFinished(QNetworkReply*)\0"
    "anId,aName\0requestFileFromNetwork(QString,QString)\0"
    "sendDataViaUdp()\0listenViaUdp()\0"
    "notify()\0processPendingDatagrams()\0"
};

void MyApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MyApp *_t = static_cast<MyApp *>(_o);
        switch (_id) {
        case 0: { QByteArray _r = _t->createXmlNdef();
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        case 1: _t->changeElementInXml((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->receivedInvokeTarget((*reinterpret_cast< const bb::system::InvokeRequest(*)>(_a[1]))); break;
        case 3: _t->onRequestFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 4: _t->requestFileFromNetwork((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->sendDataViaUdp(); break;
        case 6: _t->listenViaUdp(); break;
        case 7: _t->notify(); break;
        case 8: _t->processPendingDatagrams(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MyApp::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MyApp::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MyApp,
      qt_meta_data_MyApp, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MyApp::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MyApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MyApp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MyApp))
        return static_cast<void*>(const_cast< MyApp*>(this));
    return QObject::qt_metacast(_clname);
}

int MyApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
