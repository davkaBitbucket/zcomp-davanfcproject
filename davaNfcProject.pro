APP_NAME = davaNfcProject

CONFIG += qt warn_on cascades10
LIBS += -lQtNfcSubset -lbbsystem -lbbplatform -lbbdata -lbb
QT += network

include(config.pri)

simulator {
INCLUDEPATH += \
		${QNX_TARGET}/x86/usr/include/libxml2 \
		${QNX_TARGET}/x86/usr/include
LIBS+=	-lpackedobjects  -lxml2
}

device {

INCLUDEPATH += \
	${QNX_TARGET}/armle-v7/usr/include/libxml2 \
	${QNX_TARGET}/armle-v7/usr/include
LIBS+= -lpackedobjects -lxml2
}